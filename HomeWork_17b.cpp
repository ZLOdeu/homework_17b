﻿#include <iostream> 
using namespace std;

class Vector
{
private:
    double x;
    double y;
    double z;

public:
    Vector() : x(0), y(0), z(0)
    {}
    Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
    {}

    void VectorA()
    {
        cout << sqrt(pow(x, 2) + pow(y, 2) + pow(z, 2)) << endl;
    }

    void show()
    {
        std::cout << '\n' << x << ' ' << y << ' ' << z;
    }
};

int main()
{
    setlocale(LC_ALL, "rus");
    int x, y, z;
    cout << "Введите значение X: ";
    cin >> x;
    cout << "Введите значение Y: ";
    cin >> y;
    cout << "Введите значение Z: ";
    cin >> z;

    Vector v(x, y, z);
    v.VectorA();
    v.show();

};